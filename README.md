# Camping Beaurivage

Ce dépôt contient un logiciel de gestion de camping.

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description
Dans cette SAE, nous devons créer une application java avec interface graphique pour un camping. Cette SAE est en lien avec la SAE 2.05 qui est la gestion de ce projet. Cette SAE s'est déroulé en plusieurs étapes.

Étape de la sae : 

1. Création du diagramme de classe et du diagramme de classe par rapport à la sel
2. Maquette IHM de l'application.
3. Codage de l'application avec un lot défini par groupe, nous faisons :
    1. Consulter la liste des emplacements
    2. Afficher les emplacements ou mobil-homes libres sur une période donnée par catégorie
    3. Réaffecter un emplacement
4. Ensuite, nous avons effectué des tests d'intégration pour déceler d'éventuelles erreurs et bugs.

## Apprentissage critique
AC 11.01. Implémenter une application à partir de spécifications simples.<br>
AC 11.02. Concevoir une application (à l'aide d'UML) à partir de spécifications simples.<br>
AC 11.03. Évaluer une application (par rapport aux spécifications) par des tests.<br>
AC 11.04. Réaliser des interfaces (IHM) utilisateur.


AC 15.01 : Appréhender les besoins du client et de l’utilisateur<br>
AC 15.02 : Mettre en place les outils de gestion de projet - Utiliser un outil collaboratif<br>
AC 15.03 : Identifier les acteurs et les différentes phases d'un cycle de développement

## Langage et Framework 
  * Java
  * JavaFx

## Prérequis 
Afin de pouvoir exécuter l'application sur votre poste, vous devrez avoir :
  * Java

## Exécution
Pour lancer le projet, il faut compiler le java avec la commande suivante : `javac Main.java`.<br>
Ensuite vous laner ce projet avec cette commande : `java Main`.