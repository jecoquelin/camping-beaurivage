package Projet;

import java.time.LocalDate;

public class Emplacement {

	private static int incr=0;
	private int id;
	private String nom;
	private String cli;
	private String type;
	private LocalDate date;
	
	
	public Emplacement(String nom,String type) {
		this.id = incr;
		this.nom = nom;
		this.type = type;
		incr++;
	}
	
	public String getType() {
		return type;
	}
	
	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getCli() {
		return cli;
	}
	
	public LocalDate getDate() {
		return date;
		
	}

	
	public void setDate(LocalDate d) {
		date=d;
	}
	public void setcli(String c) {
		cli=c;
	}
}
