

import java.time.LocalDate;

import javafx.beans.binding.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableStringValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.event.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

public class FenListeEmplacement extends Stage {
	// les donn�es
	static ObservableList<String>  liste = FXCollections.observableArrayList();
	static ObservableList<Emplacement>  listeObj = FXCollections.observableArrayList();
	
	
	static ObservableList<String>  listeLibre = FXCollections.observableArrayList();
	static ObservableList<Emplacement>  listeObjLibre = FXCollections.observableArrayList();
	
	static ObservableList<String>  listeCategorie = FXCollections.observableArrayList();
	static ObservableList<Emplacement>  listeObjCategorie = FXCollections.observableArrayList();
	
	static ObservableList<String>  listeDateCat = FXCollections.observableArrayList();
	static ObservableList<Emplacement>  listeObjDateCat = FXCollections.observableArrayList();
	
	
	// les composants de la fenetre
	private AnchorPane  		racine	= new AnchorPane();
	private static ListView<String> 	listeEmplacements= new ListView<String>();
	private Button 				bnAjouter 	= new Button("Ajouter");
	private Button 				bnSupprimer = new Button("Supprimer");
	private Button 				bnReaffecter = new Button("R�affecter");
	private Button 				bnFermer = new Button("Quitter");

	
	private LocalDate todaysDate = LocalDate.now();
	private RadioButton dateRB = new RadioButton("Filtrer par date"); 
	private Label 				lblDate = new Label("Libre � la date :");
	private Label 				lblType = new Label("Type de l'emplacement :");
	private DatePicker datePicker = new DatePicker();
	
	private ChoiceBox<String> cbxLang = new ChoiceBox<String>();
	
	

	// constructeur : initialisation de la fenetre
	public FenListeEmplacement(){
		this.setTitle("Liste des emplacements");
		this.initval();
		this.setMinWidth(500);
		this.setMinHeight(313);
		this.sizeToScene();
		this.setResizable(true);
		this.setScene(new Scene(creerContenu()));
	}
	
	
	private Parent creerContenu() {
		listeEmplacements.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		listeEmplacements.setItems(liste);
		listeEmplacements.setFixedCellSize(40.0);
		
		BooleanBinding selectionVide = Bindings.equal(-1, listeEmplacements.getSelectionModel().selectedIndexProperty());
		
		
		bnSupprimer.disableProperty().bind(selectionVide);
		bnReaffecter.disableProperty().bind(selectionVide);
		
		// detection et traitement des evenements
		bnAjouter.setPrefWidth(100);
		bnAjouter.setOnAction(e -> EntrerEmplacement());
		bnSupprimer.setPrefWidth(100);
		bnSupprimer.setOnAction(e -> supprimer());
		bnReaffecter.setPrefWidth(100);
		bnFermer.setPrefWidth(100);
		
		bnReaffecter.setOnAction(e -> {
			Stage ajout;
    		String nomcli=listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).getCli();
    		LocalDate date=listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).getDate();
    		String type=listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).getType();
    		ajout = new FenAffecter(nomcli,date,type);
    		ajout.show();
		});
		
		
		dateRB.setOnAction(e-> SetLibre());
		

	    datePicker.setValue(todaysDate);
	    datePicker.setShowWeekNumbers(false);
	    datePicker.setPrefWidth(100);
	    
	    cbxLang.setPrefWidth(100);
		cbxLang.getItems().addAll("Tout","Emplacement simple", "Mobil-home standard", "Mobil-home de luxe");
		cbxLang.getSelectionModel().select(0);
		cbxLang.setOnAction(e-> SetCategorie(cbxLang.getSelectionModel().getSelectedItem()));
	    		
		listeEmplacements.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent click) {
		        if (click.getClickCount() == 2 && listeEmplacements.getSelectionModel().getSelectedIndex()!=-1) {
		    		Stage ajout;
		    		String nomcli=listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).getCli();
		    		LocalDate date=listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).getDate();
		    		String type=listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).getType();
		    		ajout = new FenAffecter(nomcli,date,type);
		    		ajout.show();	    		
		        }
		    }
		});
		bnFermer.setOnAction(e -> System.exit(0));
			
		// creation du Scene graph
		
		AnchorPane.setBottomAnchor(listeEmplacements, 10.0);
		AnchorPane.setLeftAnchor(listeEmplacements, 10.0);
		AnchorPane.setTopAnchor(listeEmplacements, 10.0);
		AnchorPane.setRightAnchor(listeEmplacements, 240.0);
		
		VBox zoneDroite = new VBox();
		HBox zoneDate = new HBox();
		zoneDroite.setPadding(new Insets(10));
		zoneDroite.setAlignment(Pos.TOP_RIGHT);
		zoneDroite.setSpacing(10.0);
		zoneDate.getChildren().addAll(dateRB,datePicker);
		lblDate.setAlignment(Pos.BOTTOM_LEFT);
		zoneDate.setSpacing(10.0);
		zoneDroite.getChildren().addAll(bnAjouter,bnReaffecter,bnSupprimer,lblDate,zoneDate,lblType,cbxLang);
		
		AnchorPane.setRightAnchor(zoneDroite, 0.0);
		AnchorPane.setTopAnchor(zoneDroite, 0.0);
		AnchorPane.setBottomAnchor(bnFermer, 10.0);
		AnchorPane.setRightAnchor(bnFermer, 10.0);
		
		
		racine.getChildren().addAll(listeEmplacements,zoneDroite,bnFermer);
		
		
		
		return racine;
	}
	
	
	public void initval() {
		Emplacement emp1 = new Emplacement("Lumi�re","Emplacement simple");
		Emplacement emp2 = new Emplacement("Papillon","Mobil-home standard");
		Emplacement emp3 = new Emplacement("Soleil","Mobil-home de luxe");
		Emplacement emp4 = new Emplacement("Nuage","Emplacement simple");
		liste.add("Nom: "+emp1.getNom()+"\nID: "+emp1.getId());
		liste.add("Nom: "+emp2.getNom()+"\nID: "+emp2.getId());
		liste.add("Nom: "+emp3.getNom()+"\nID: "+emp3.getId());
		liste.add("Nom: "+emp4.getNom()+"\nID: "+emp4.getId());
		listeObj.addAll(emp1,emp2,emp3,emp4);
	}

	
	
	public static void AjoutEmplacement(String nom,String type) {
		Emplacement emp = new Emplacement(nom,type);
		liste.add("Nom: "+emp.getNom()+"\nID: "+emp.getId());
		listeObj.add(emp);
	}
	
	public void EntrerEmplacement(){
		Stage ajout;
		ajout = new FenAjout();
		ajout.show();
	}
	
	
	public void supprimer() {
		if (listeEmplacements.getSelectionModel().getSelectedIndex()!=-1) {
			liste.remove(listeEmplacements.getSelectionModel().getSelectedIndex());
			listeObj.remove(listeEmplacements.getSelectionModel().getSelectedIndex());
		}
	}
	
	
	
	public static void affecter(String c,LocalDate d) {
		String cli=c;
		listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).setcli(cli);
		LocalDate date=d;
		listeObj.get(listeEmplacements.getSelectionModel().getSelectedIndex()).setDate(date);
	}
	
	
	
	public void SetLibre() {
		
		if(dateRB.isSelected()) {
			
			if(cbxLang.getSelectionModel().getSelectedItem().compareTo(("Tout"))==0) {
				
				for(int i=0;i<listeObj.size();i++) {
					if((listeObj.get(i).getDate()+"").compareTo(datePicker.getValue()+"")!=0) {
						listeObjLibre.add(listeObj.get(i));
					}
				}
			
				for(int i=0;i<listeObjLibre.size();i++) {
					listeLibre.add("Nom: "+listeObjLibre.get(i).getNom()+"\nID: "+listeObjLibre.get(i).getId());
				}
			
				listeEmplacements.setItems(listeLibre);
				

			}else {
				setDateCat(cbxLang.getSelectionModel().getSelectedItem());
			}
	
		}else {
			
			listeLibre.removeAll(listeLibre);
			listeObjLibre.removeAll(listeObjLibre);
			listeEmplacements.setItems(liste);
		}
	}
	
	
	
public void SetCategorie(String c) {
		
		listeObjCategorie.removeAll(listeObjCategorie);
		listeCategorie.removeAll(listeCategorie);
		
		if(c.compareTo("Tout")!=0){
			if(!dateRB.isSelected()) {
				for(int i=0;i<listeObj.size();i++) {
					if((listeObj.get(i).getType()+"").compareTo(c)==0) {
						listeObjCategorie.add(listeObj.get(i));
					}
				
				}
			
				for(int i=0;i<listeObjCategorie.size();i++) {
					listeCategorie.add("Nom: "+listeObjCategorie.get(i).getNom()+"\nID: "+listeObjCategorie.get(i).getId());
				listeEmplacements.setItems(listeCategorie);
				}
			
			}else {
				setDateCat(c);
			}
			
		}else {
			listeEmplacements.setItems(liste);
		}
	}


	public void setDateCat(String c){
		
		
		listeObjDateCat.removeAll(listeObjDateCat);
		listeDateCat.removeAll(listeDateCat);			
		for(int i=0;i<listeObj.size();i++) {
			if(((listeObj.get(i).getDate()+"").compareTo(datePicker.getValue()+"")!=0) && (listeObj.get(i).getType()+"").compareTo(c)==0) {
				listeObjDateCat.add(listeObj.get(i));
			}
		}
		
		for(int i=0;i<listeObjDateCat.size();i++) {
			listeDateCat.add("Nom: "+listeObjDateCat.get(i).getNom()+"\nID: "+listeObjDateCat.get(i).getId());
		}
		listeEmplacements.setItems(listeDateCat);
	}
}



