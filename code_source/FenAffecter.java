package Projet;

import java.time.LocalDate;
import java.util.Date;

import javafx.beans.binding.*;

import javafx.stage.Stage;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.geometry.*;

public class FenAffecter extends Stage{
	
	private String nom;
	private Date date;
	private TextField 	txtNom = new TextField();
	private DatePicker datePicker = new DatePicker();
	private Label 			lblType 		= new Label("Type : ");
	private Label 			lbldefType		= new Label();
	
	
	public FenAffecter(String nom,LocalDate date,String cat)  {
		// les composants du formulaire
		Label 			lblNom 		= new Label("Nom du client: ");
		Label 			lblDate 	= new Label("Date de réservation: ");
		Button 			bnOk 	= new Button("Ok");
		Button 			bnAnnuler 	= new Button("Annuler");
		lbldefType.setText(cat);
		txtNom.setText(nom);
		txtNom.setMaxWidth(200);
		datePicker.setValue(date);
		datePicker.setMaxWidth(200);

		bnOk.setOnAction(e -> this.affecter());
		bnOk.setPrefWidth(100);
		bnAnnuler.setOnAction(e -> this.close());
		bnAnnuler.setPrefWidth(100);
		
		

		
		GridPane grid = new GridPane();
		
		HBox zoneBoutons = new HBox();
		
		zoneBoutons.getChildren().addAll(bnOk,bnAnnuler);
		zoneBoutons.setSpacing(10);
		zoneBoutons.setPadding(new Insets(10));
		zoneBoutons.setAlignment(Pos.BOTTOM_RIGHT);
		
		grid.addRow(0, lblNom,txtNom);
		grid.addRow(1, lblDate,datePicker);
		grid.addRow(2, lblType,lbldefType);
		grid.add(zoneBoutons, 1, 3);
		grid.setVgap(10);
		grid.setHgap(10.0);
		grid.setPadding(new Insets(10));
		Scene scene = new Scene(grid);
		this.setTitle("Affecter emplacement");
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(400);
		this.show();
	}
	
	
	private void affecter() {
		FenListeEmplacement.affecter(txtNom.getText(), datePicker.getValue());
		this.close();
	}


	
}
















