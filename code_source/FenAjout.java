package Projet;

import javafx.beans.binding.*;
import javafx.stage.Stage;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;

public class FenAjout extends Stage{
	private Label 			lblNom 		= new Label("Nom de l'emplacement: ");
	private Label 			lblType 		= new Label("Type de l'emplacement: ");
	private TextField 		txtNom		= new TextField();
	private Button 			bnAjouter 	= new Button("Ajouter");
	private Button 			bnAnnuler 	= new Button("Annuler");
	private ChoiceBox<String> cbxLang = new ChoiceBox<String>();
	public FenAjout()  {
		
		
		bnAjouter.disableProperty().bind(
				Bindings.when(txtNom.textProperty().isEmpty()).
				then(true).otherwise(false)
				);
		

		
		bnAnnuler.setOnAction(e -> this.close());
		bnAnnuler.setPrefWidth(100);
		bnAjouter.setOnAction(e -> Ajouter());
		bnAjouter.setPrefWidth(100);
		
		
		cbxLang.getItems().addAll("Emplacement simple", "Mobil-home standard", "Mobil-home de luxe");
		cbxLang.getSelectionModel().select(0);
		
		
		GridPane grid = new GridPane();
		HBox zoneBoutons = new HBox();
		HBox zoneHaut = new HBox();
		HBox zoneBas = new HBox();
		
		
		zoneBoutons.getChildren().addAll(bnAjouter,bnAnnuler);
		zoneBoutons.setSpacing(10);
		zoneBoutons.setPadding(new Insets(10));
		zoneBoutons.setAlignment(Pos.BOTTOM_RIGHT);
		
		zoneBas.getChildren().addAll(lblType,cbxLang);
		zoneBas.setSpacing(10);
		zoneBas.setPadding(new Insets(10));
		
		zoneHaut.getChildren().addAll(lblNom,txtNom);
		zoneHaut.setSpacing(10);
		zoneHaut.setPadding(new Insets(10));
		
		grid.add(zoneHaut, 0, 0);
		grid.add(zoneBas, 0, 1);
		grid.add(zoneBoutons, 0, 2);
		grid.setVgap(10);
		Scene scene = new Scene(grid);
		this.setTitle("Ajouter Emplacement");
		this.setScene(scene);
		this.setResizable(false);
		this.show();
	}
	
	
	
	
	
	public void Ajouter() {
		FenListeEmplacement.AjoutEmplacement(this.txtNom.getText(),this.cbxLang.getSelectionModel().getSelectedItem());
		this.close();
	}
	
	
	
	
	
	
}
















